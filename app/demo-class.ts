// creating a class
class Developer {
    // properties
    department: string;
    private _title: string;
    // accessor methods: allows to change the value of class member properties
    get title(): string {
        return this._title;
    }
    set title(newTitle: string) {
        this._title = newTitle.toUpperCase();
    }

    // methods
    documentRequirements(requirements: string) {
        console.log(requirements);
    }
}

// inheriting from the Developer class
class WebDeveloper extends Developer {
    // sub class properties
    favouriteEditor: string;
    // sub class methods
    writeCode(): string{
        return `Writing typescript code`
    }
}

// creating an object of WebDeveloper class
let webDev: WebDeveloper = new WebDeveloper();
webDev.department = "Engineering";
webDev.favouriteEditor = "VS code";
webDev.writeCode();

// implementing an interface
interface Employee {
    name: string;
    title: string;
    showInfo(): string;
}

// implementing an interface
class Engineer implements Employee {
    name: string;
    title: string;
    showInfo(): string {
        return `${this.name}-${this.title}`
    }
}

let harry: Engineer = new Engineer();
harry.name = "Harry Gibson";
harry.title = "Developer";
harry.showInfo();